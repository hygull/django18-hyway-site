from django.conf.urls import include, url, patterns
from django.contrib import admin

urlpatterns = [
    url(r'^$', 'HyWayApp.views.home', name='home'),
    url(r'^myway/', include('HyWayApp.urls')),

    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += patterns('',
    url(r'^captcha/', include('captcha.urls')),
)