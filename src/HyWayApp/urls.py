from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
	url("^contact/$","HyWayApp.views.contact",name="hyway_contact"),
	url("^seller/register/$","HyWayApp.views.seller",name="hyway_seller"),

	#Urls for success messages
	url("^success/$","HyWayApp.views.success",name="hyway_success"),
	
	#Urls for error messages
	url("^error/$","HyWayApp.views.error",name="hyway_error"),
]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
