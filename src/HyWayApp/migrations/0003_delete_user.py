# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('HyWayApp', '0002_auto_20170317_1351'),
    ]

    operations = [
        migrations.DeleteModel(
            name='User',
        ),
    ]
