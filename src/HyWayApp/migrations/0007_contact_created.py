# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('HyWayApp', '0006_contact'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2017, 3, 22, 7, 51, 52, 104258, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
