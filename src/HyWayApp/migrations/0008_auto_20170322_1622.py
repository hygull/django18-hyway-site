# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('HyWayApp', '0007_contact_created'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='fullname',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='contact',
            name='message',
            field=models.TextField(null=True, blank=True),
        ),
    ]
