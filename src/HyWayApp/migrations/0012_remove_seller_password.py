# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('HyWayApp', '0011_seller_password'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='seller',
            name='password',
        ),
    ]
