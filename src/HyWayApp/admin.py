from django.contrib import admin

from .models import Seller,Contact

class SellerAdmin(admin.ModelAdmin):
	model = Seller
	list_display = ["__unicode__","email","contact_number","created","updated","address"]


class ContactAdmin(admin.ModelAdmin):
	model = Contact
	list_display = ["fullname","email","message","created"]


admin.site.register(Seller,SellerAdmin)
admin.site.register(Contact,ContactAdmin)