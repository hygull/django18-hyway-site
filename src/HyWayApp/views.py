from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render,redirect
from .forms import ContactForm, SellerForm
from django.conf import settings
from django.core.mail import send_mail

#**********************************************************************************
def home(request):
	"""A view function that renders home page"""
	return render(request,"hyway/home.html",{})


#**********************************************************************************
def send_email_to(instance):
	"""A view function that sends emails to those who contacts the site owner"""
	html_msg = 	"""
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <center><h2 style="color:green;font-weight:bold;">Desham</h2><hr>
  <p style="color:gray;font-weight:bold;font-family:helvetica;">A new contact message</p>  
       
  <table class="table table-bordered" style='border-collapse:collapse;'>
    <thead>
      <tr>
        <th  style='border:1px solid green;padding:10px;color:blue'>Full name</th>
        <th  style='border:1px solid green;padding:10px;color:blue'>Email</th>
        <th  style='border:1px solid green;padding:10px;color:blue'>Message</th>
      </tr>
    </thead>
    <tbody>
      <tr>
       <td style='border:1px solid green;padding:10px;color:green'>%s</td>
       <td  style='border:1px solid green;padding:10px;color:green'>%s</td>
       <td  style='border:1px solid green;padding:10px;color:green'>%s</td>
      </tr>
    </tbody>
  </table>
  </center>   
</div>

</body>
</html>
"""%(instance.fullname,instance.email,instance.message)
	send_mail("Contact Message:Desham","", settings.EMAIL_HOST_USER, [settings.EMAIL_HOST_USER], fail_silently=False, html_message=html_msg)
	# html_msg2="""<center>
	# 	<h1 style='color:gray;font-weight:bold;font-family:helvetica;'>Desham</h1>
	# 	<p>

	# 	</p>
	# </center>"""
	send_mail("Desham","Dear "+str(instance.fullname)+",\nThanks for contacting us.\n\nWe are inviting you in our site to sell your products if you have.\n\nYou can also buy products online from here.\n\nRegards,\n\t\tSurya", settings.EMAIL_HOST_USER, [instance.email], fail_silently=True)
	print "email successfully sent"

#**********************************************************************************
def send_email_to_seller(instance):
	"""A function that sends emails to seller"""
	html_msg = 	"""
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <center><h2 style="color:green;font-weight:bold;">Desham</h2><hr>
  <p style="color:gray;font-weight:bold;font-family:helvetica;">A new contact message</p>  
       
  <table class="table table-bordered" style='border-collapse:collapse;'>
    <thead>
      <tr>
        <th  style='border:1px solid green;padding:10px;color:blue'>Full name</th>
        <th  style='border:1px solid green;padding:10px;color:blue'>Email</th>
        <th  style='border:1px solid green;padding:10px;color:blue'>Message</th>
      </tr>
    </thead>
    <tbody>
      <tr>
       <td style='border:1px solid green;padding:10px;color:green'>%s</td>
       <td  style='border:1px solid green;padding:10px;color:green'>%s</td>
       <td  style='border:1px solid green;padding:10px;color:green'>%s</td>
      </tr>
    </tbody>
  </table>
  </center>   
</div>

</body>
</html>
"""%(instance.firstname+" "+instance.lastname,instance.email,instance.contact_number)
	send_mail("New Seller Registration","", settings.EMAIL_HOST_USER, [settings.EMAIL_HOST_USER], fail_silently=False, html_message=html_msg)
	# html_msg2="""<center>
	# 	<h1 style='color:gray;font-weight:bold;font-family:helvetica;'>Desham</h1>
	# 	<p>

	# 	</p>
	# </center>"""
	send_mail("Desham","Dear "+str(instance.firstname +" "+ instance.lastname)+",\nThanks for joining us.\n\nNow you can upload the details of your product on our site to sell it online.\n\nVisit our site and upload your product details\n\nYou can also buy products online from here if you want.\n\nRegards,\n\t\tSurya", settings.EMAIL_HOST_USER, [instance.email], fail_silently=True)
	print "email successfully sent"

#**********************************************************************************
def contact(request):
	"""A view function that renders Contact form"""
	form = ContactForm()
	context = {}
	instance = form.save(commit=False)
	if request.method == "POST":
		form = ContactForm(request.POST or None)
		if form.is_valid():
			print "Contact form data is now valid"
			print "Sending email"
			try:
				instance = form.save(commit=True)
				send_email_to(instance)
			except Exception as e:
				print "Error while sending email",e

			form = ContactForm() #Clear form after submission
			context["form"] = form
		else:
			print "Contact form data is not valid"
			context["form"] = form
	else:
		print "Initial form is going to be rendered"
		context["form"] = form
	return render(request,"hyway/contact.html",context)

#**********************************************************************************
def seller(request):
	"""A view function that renders Seller Registration form"""
	form = SellerForm()
	context = {}
	
	if request.method == "POST":
		form = SellerForm(request.POST or None)
		if form.is_valid():
			instance = form.save(commit=False)
			context["form"] = form
			print "Seller form data is now valid"
			print "Sending email"

			try:
				instance = form.save(commit=True)
				send_email_to_seller(instance)
			except Exception as e:
				print "Error while sending email",e

			# form = SellerForm() #Clear form after submission
			# context["form"] = form
			return redirect("/myway/success/")
			#return HttpResponseRedirect(reverse("hyway_success",message="Great!!!"))
		else:
			print "Seller form data is not valid"
			context["form"] = form
	else:
		print "Initial form is going to be rendered"
		context["form"] = form

	context["form"] = form #Remove on uncomment
	return render(request,"hyway/seller.html",context)

#**********************************************************************************
def success(request):
	return render(request,"hyway/success.html",{"message":"<h5 style='color:green;'>Thanks for registering as a seller </h5><h6 style='color:navy;font-family:helvetica;'>Now you can login and add your product details to our site</h6><small>Check your mail box <a target='_blank' href='https://mail.google.com/'>here</a> for more details</small>"})

#**********************************************************************************
def error(request):
	return render(request,"hyway/error.html")

#**********************************************************************************

#**********************************************************************************
